﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.lblText = New System.Windows.Forms.Label()
        Me.btnGreetings = New System.Windows.Forms.Button()
        Me.btnGoodbye = New System.Windows.Forms.Button()
        Me.btnHello = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lblText
        '
        Me.lblText.Location = New System.Drawing.Point(65, 29)
        Me.lblText.Name = "lblText"
        Me.lblText.Size = New System.Drawing.Size(144, 66)
        Me.lblText.TabIndex = 0
        Me.lblText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnGreetings
        '
        Me.btnGreetings.Location = New System.Drawing.Point(102, 171)
        Me.btnGreetings.Name = "btnGreetings"
        Me.btnGreetings.Size = New System.Drawing.Size(75, 23)
        Me.btnGreetings.TabIndex = 1
        Me.btnGreetings.Text = "Greetings"
        Me.btnGreetings.UseVisualStyleBackColor = True
        '
        'btnHello
        '
        Me.btnHello.Location = New System.Drawing.Point(197, 171)
        Me.btnHello.Name = "btnHello"
        Me.btnHello.Size = New System.Drawing.Size(75, 23)
        Me.btnHello.TabIndex = 2
        Me.btnHello.Text = "Hello"
        Me.btnHello.UseVisualStyleBackColor = True
        '
        'btnGoodbye
        '
        Me.btnGoodbye.Location = New System.Drawing.Point(12, 171)
        Me.btnGoodbye.Name = "btnGoodbye"
        Me.btnGoodbye.Size = New System.Drawing.Size(75, 23)
        Me.btnGoodbye.TabIndex = 2
        Me.btnGoodbye.Text = "Goodbye"
        Me.btnGoodbye.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 261)
        Me.Controls.Add(Me.btnGoodbye)
        Me.Controls.Add(Me.btnHello)
        Me.Controls.Add(Me.btnGreetings)
        Me.Controls.Add(Me.lblText)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents lblText As Label
    Friend WithEvents btnGreetings As Button
    Friend WithEvents btnGoodbye As Button
    Friend WithEvents btnHello As Button
End Class
